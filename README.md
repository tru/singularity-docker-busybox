# building a busybox toy system for singularity and docker image for CI

Tru <tru@pasteur.fr>

## Why ?
- toy system for gitlab-CI
- build docker image from dockerhub registry and push to registry.pasteur.fr with proper tags
- build docker image, push to registry.pasteur.fr and re-use that docker image to create a singularity container as artefact
- 
## Caveat
- playground, use at your own risk!
- `:main` tagged docker image
- `:latest` tagged singularity image

## Usage:

Docker:
```
docker run -ti registry-gitlab.pasteur.fr/tru/singularity-docker-busybox:main
```
Singularity:
```
singularity run oras://registry-gitlab.pasteur.fr/tru/singularity-docker-busybox:latest
```
